<?php

namespace Drupal\media_entity_issuu\Plugin\Validation\Constraint;

use Drupal\media_entity\EmbedCodeValueTrait;
use Drupal\media_entity_issuu\Plugin\MediaEntity\Type\Issuu;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the IssuuEmbedCode constraint.
 */
class IssuuEmbedCodeConstraintValidator extends ConstraintValidator {

  use EmbedCodeValueTrait;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    $value = $this->getEmbedCode($value);
    if (!isset($value)) {
      return;
    }

    $post_url = Issuu::parseIssuuEmbedField($value);
    if ($post_url === FALSE) {
      $this->context->addViolation($constraint->message);
    }
  }

}
