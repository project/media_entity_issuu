<?php

namespace Drupal\media_entity_issuu\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\media_entity_issuu\IssuuMarkup;
use Drupal\media_entity_issuu\Plugin\MediaEntity\Type\Issuu;

/**
 * Plugin implementation of the 'issuu_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "issuu_embed",
 *   label = @Translation("Issuu embed"),
 *   field_types = {
 *     "link", "string", "string_long"
 *   }
 * )
 */
class IssuuEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\media_entity\MediaInterface $media_entity */
    $media_entity = $items->getEntity();

    $element = [];
    if (($type = $media_entity->getType()) && $type instanceof Issuu) {
      foreach ($items as $delta => $item) {
        $element[$delta] = [
          '#markup' => IssuuMarkup::create($type->getField($media_entity, 'html')),
          '#attached' => [
            'library' => 'media_entity_issuu/issuu.embed'
          ]
        ];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() === 'media';
  }

}
