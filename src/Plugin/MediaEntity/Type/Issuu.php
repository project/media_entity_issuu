<?php

namespace Drupal\media_entity_issuu\Plugin\MediaEntity\Type;

use Drupal\Core\Form\FormStateInterface;
use Drupal\media_entity\MediaInterface;
use Drupal\media_entity\MediaTypeBase;
use GuzzleHttp\Exception\TransferException;

/**
 * Provides media type plugin for Issuu.
 *
 * @MediaType(
 *   id = "issuu",
 *   label = @Translation("Issuu"),
 *   description = @Translation("Provides business logic and metadata for Issuu.")
 * )
 *
 * @todo On the long run we could switch to the issuu API which provides WAY
 *   more fields.
 */
class Issuu extends MediaTypeBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'source_field' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $options = [];
    $bundle = $form_state->getFormObject()->getEntity();
    $allowed_field_types = ['string', 'string_long', 'link'];
    foreach ($this->entityFieldManager->getFieldDefinitions('media', $bundle->id()) as $field_name => $field) {
      if (in_array($field->getType(), $allowed_field_types) && !$field->getFieldStorageDefinition()->isBaseField()) {
        $options[$field_name] = $field->getLabel();
      }
    }

    $form['source_field'] = [
      '#type' => 'select',
      '#title' => t('Field with source information'),
      '#description' => t('Field on media entity that stores issuu embed code or URL. You can create a bundle without selecting a value for this dropdown initially. This dropdown can be populated after adding fields to the bundle.'),
      '#default_value' => empty($this->configuration['source_field']) ? NULL : $this->configuration['source_field'],
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function providedFields() {
    return [
//      'width',
//      'height',
//      'id',
//      'html',
    ];
  }

  /**
   * Returns the embed data for a Issuu post.
   *
   * @param string $code
   *   The URL to the issuu post.
   *
   * @return bool|array
   *   FALSE if there was a problem retrieving the oEmbed data, otherwise
   *   an array of the data is returned.
   */
  protected function defaultEmbed($code) {
    $string = '<div data-configid="@code" class="issuuembed"></div>';

    return str_replace('@code', $code, $string);
  }

  /**
   * Runs preg_match on embed code/URL.
   *
   * @param MediaInterface $media
   *   Media object.
   *
   * @return string|false
   *   The issuu url or FALSE if there is no field or it contains invalid
   *   data.
   */
  protected function getIssuuUrl(MediaInterface $media) {
    if (isset($this->configuration['source_field'])) {
      $source_field = $this->configuration['source_field'];

      if ($media->hasField($source_field)) {
        $property_name = $media->{$source_field}->first()->mainPropertyName();
        $embed = $media->{$source_field}->{$property_name};

        return static::parseIssuuEmbedField($embed);
      }
    }

    return FALSE;
  }

  /**
   * Extract a Issuu content URL from a string.
   *
   * Typically users will enter an iframe embed code that Issuu provides, so
   * which needs to be parsed to extract the actual post URL.
   *
   * Users may also enter the actual content URL - in which case we just return
   * the value if it matches our expected format.
   *
   * @param string $data
   *   The string that contains the Issuu post URL.
   *
   * @return string|bool
   *   The post URL, or FALSE if one cannot be found.
   */
  public static function parseIssuuEmbedField($data) {
    $data = trim($data);

    // Ideally we would verify that the content URL matches an exact pattern,
    // but Issuu has a ton of different ways posts/notes/videos/etc URLs can
    // be formatted, so it's not practical to try and validate them. Instead,
    // just validate that the content URL is from the issuu domain.
    $content_url_regex = '/^http:\/\/(www\.)?issuu\.com\//i';
    $default_regex = '/data-configid="(\d*\/\d*)"/';
    $item_regex = '/\d*\/\d*/';

    if (preg_match($content_url_regex, $data)) {
      $uri_parts = parse_url($data);

      if ($uri_parts !== FALSE && isset($uri_parts['query'])) {
        parse_str($uri_parts['query'], $query_params);

        if (isset($query_params['e']) && preg_match($item_regex, $query_params['e'])) {
          return $query_params['e'];
        }
      }

      return $data;
    }

    if (preg_match($default_regex, $data, $matches)) {
      if (!empty($matches[1])) {
        return $matches[1];
      }
    }
    else {
      // Check if the user entered an iframe embed instead, and if so,
      // extract the post URL from the iframe src.
      $doc = new \DOMDocument();
      if (@$doc->loadHTML($data)) {
        $iframes = $doc->getElementsByTagName('iframe');

        if ($iframes->length > 0 && $iframes->item(0)->hasAttribute('src')) {
          $iframe_src = $iframes->item(0)->getAttribute('src');
          $uri_parts = parse_url($iframe_src);

          if ($uri_parts !== FALSE && isset($uri_parts['fragment'])) {
            parse_str($uri_parts['fragment'], $query_params);
            $fragment = array_keys($query_params);

            if (isset($fragment[0]) && preg_match($item_regex, $fragment[0])) {
              return $fragment[0];
            }
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getField(MediaInterface $media, $name) {
    $content_id = $this->getIssuuUrl($media);

    if ($content_id === FALSE) {
      return FALSE;
    }

    switch ($name) {
//      case 'width':
//        return $data['width'];
//
//      case 'height':
//        return $data['height'];
//
      case 'id':
        return $content_id;

      case 'html':
        return $this->defaultEmbed($content_id);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function thumbnail(MediaInterface $media) {
    // @todo Add support for thumbnails on the long run.
    return $this->getDefaultThumbnail();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultThumbnail() {
    return $this->config->get('icon_base') . '/issuu.png';
  }

  /**
   * {@inheritdoc}
   */
  public function attachConstraints(MediaInterface $media) {
    parent::attachConstraints($media);

    if (isset($this->configuration['source_field'])) {
      $source_field_name = $this->configuration['source_field'];
      if ($media->hasField($source_field_name)) {
        foreach ($media->get($source_field_name) as &$embed_code) {
          /** @var \Drupal\Core\TypedData\DataDefinitionInterface $typed_data */
          $typed_data = $embed_code->getDataDefinition();
          $typed_data->addConstraint('IssuuEmbedCode');
        }
      }
    }
  }

}
