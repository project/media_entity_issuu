<?php

namespace Drupal\media_entity_issuu;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Render\MarkupTrait;

class IssuuMarkup implements MarkupInterface {

  use MarkupTrait;

}
